package com.example.demoboost.feature.intro

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import com.example.demoboost.R


class AnswerBar @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : View(context, attrs) {

    private var xPointer: Float = 0F
    private var yPointer: Float = 0F
    private var answerBarColor: IntArray? = null
    private var answerBarColorPosition: FloatArray = floatArrayOf(0F, 0.4F, 0.6F, 1F)

    private var paddingHorizontal: Float = 50.dpToPx()
        set(value) {
            field = value
            invalidate()
        }
    private var selectorSize: Float = 24.dpToPx()
        set(value) {
            field = value
            invalidate()
        }
    private var strokeWidth: Float = 10.dpToPx()
        set(value) {
            field = value
            invalidate()
        }
    private var levelRate: Int = 3
        set(value) {
            field = value
            invalidate()
        }
        get() = if (field <= 0) 1 else field
    private var selectorIcon: Drawable = resources.getDrawable(R.drawable.ic_ice)
        set(value) {
            field = value
            invalidate()
        }

    private var pixelPerLevel: Float = 0F
    private var xSelector: Float = 0F
        get() = xPointer - paddingHorizontal

    private var ySelector: Float = 0F
        get() = yPointer

    private var minRateSelector: Float = 0F
        get() = paddingHorizontal

    private var maxRateSelector: Float = width.toFloat()
        get() = width - paddingHorizontal

    private val mLinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
        strokeWidth = this@AnswerBar.strokeWidth
    }
    private val mGradientPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
        strokeWidth = this@AnswerBar.strokeWidth
    }
    private val mTrianglePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = Color.WHITE
    }
    private val mTextPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textAlign = Paint.Align.CENTER
        textSize = 20.dpToPx()
    }


    init {
        context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.AnswerBar,
                0, 0).apply {
            try {
                paddingHorizontal = getDimension(R.styleable.AnswerBar_abPaddingHorizontal, 50.dpToPx())
                selectorSize = getDimension(R.styleable.AnswerBar_abSelectorSize, 24.dpToPx())
                strokeWidth = getDimension(R.styleable.AnswerBar_abStrokeWidth, 10.dpToPx())
                levelRate = getInteger(R.styleable.AnswerBar_abLevelRate, 1)
                selectorIcon = getDrawable(R.styleable.AnswerBar_abSelectorIcon)
                answerBarColor = resources.getIntArray(R.array.anwserBar)
            } finally {
                recycle()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val w: Int = View.resolveSizeAndState(width, widthMeasureSpec, 1)
        val h: Int = View.resolveSizeAndState(
                View.MeasureSpec.getSize(w),
                heightMeasureSpec,
                0
        )
        setMeasuredDimension(w, h)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateMeasure()
    }

    private fun updateMeasure() {
        yPointer = height / 2.toFloat()
        pixelPerLevel = (width - paddingHorizontal * 2) / levelRate
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        when {
            xPointer < minRateSelector -> {
                xPointer = minRateSelector
            }
            xPointer > maxRateSelector -> {
                xPointer = maxRateSelector
            }
        }

        canvas.drawLine(minRateSelector, ySelector, maxRateSelector, ySelector, mLinePaint.apply {
            color = resources.getColor(R.color.colorBackgroundAnswerBar)
        })
        canvas.drawLine(minRateSelector, ySelector, xPointer, ySelector, mGradientPaint.apply {
            shader = LinearGradient(minRateSelector, ySelector, maxRateSelector, ySelector, answerBarColor, answerBarColorPosition, Shader.TileMode.CLAMP)
        })

        for (i in 0..levelRate) {
            if (i in 1 until levelRate) {
                val position = minRateSelector + i * pixelPerLevel
                drawTriangle(canvas, position, ySelector - 10, true)
                drawTriangle(canvas, position, ySelector + 10)
            }
            canvas.drawText(i.toString(), minRateSelector + i * pixelPerLevel,
                    ySelector + (this@AnswerBar.strokeWidth * 1.5).toInt(), mTextPaint)
        }

        selectorIcon.setBounds((xPointer - selectorSize / 2).toInt(), (yPointer - selectorSize / 2).toInt(),
                (xPointer + selectorSize / 2).toInt(), (yPointer + selectorSize / 2).toInt())
        selectorIcon.draw(canvas)
    }

    private fun drawTriangle(canvas: Canvas, x: Float, y: Float, isRevert: Boolean = false) {
        val path = Path()
        path.moveTo(x - 8, y)
        path.lineTo(x + 8, y)
        path.lineTo(x, if (isRevert) y + 8 else y - 8)
        path.lineTo(x - 8, y)
        path.close()
        canvas.drawPath(path, mTrianglePaint)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val x: Float = event?.x ?: 0F
        when (event?.action) {
            MotionEvent.ACTION_UP, MotionEvent.ACTION_MOVE -> {
                if(getCurrentLevel(xPointer)!=getCurrentLevel(x)){
                    xPointer = minRateSelector + pixelPerLevel * getCurrentLevel(x)
                    invalidate()
                }
            }
        }
        return true
    }

    private fun getCurrentLevel(xPosition: Float? = xPointer): Int {
        return xPosition?.let {
            var level: Int = ((xPosition - minRateSelector) / pixelPerLevel).toInt()
            if ((xPosition - minRateSelector) % pixelPerLevel > pixelPerLevel / 2) {
                level += 1
            }
            level
        } ?: 0
    }

    fun getLevelSelected(isRevest: Boolean = false): Int {
        return if (isRevest) {
            levelRate - getCurrentLevel()
        } else {
            getCurrentLevel()
        }
    }

    private fun Int.dpToPx(): Float {
        val displayMetrics = resources.displayMetrics
        return Math.round(this * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).toFloat()
    }

}