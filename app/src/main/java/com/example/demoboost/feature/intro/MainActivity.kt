package com.example.demoboost.feature.intro

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.text.Html
import android.widget.TextView
import com.example.demoboost.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var slideAdapter: SlideAdapter

    private lateinit var dots: Array<TextView?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        slideAdapter = SlideAdapter()

        view_pager_intro.adapter = slideAdapter

        addDotsIndicator(0)

        view_pager_intro.addOnPageChangeListener(viewListener)
    }

    fun addDotsIndicator(position: Int){
        dots = arrayOfNulls(3)
        dot_layout.removeAllViews()
        for (i in 0..2){
            dots[i] = TextView(this)
            dots[i]?.text = Html.fromHtml("&#8226;")

            dots[i]?.textSize = 35F
            dots[i]?.setTextColor(resources.getColor(R.color.transparence))

            dot_layout.addView(dots[i])
        }

        if(dots.isNotEmpty()){
            dots[position]?.setTextColor(resources.getColor(R.color.colorPrimary))
        }
    }

    val viewListener = object: ViewPager.OnPageChangeListener{
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            addDotsIndicator(position)
        }

    }
}
