package com.example.demoboost.feature.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import com.example.demoboost.R

class EditTextCus : AppCompatEditText {

    lateinit var deleteButtonImage: Drawable

    var isCorrect = false

    var regex: String? = ""

    var regexx: Regex? = null

    constructor(context: Context) : super(context) {
        setupButton(R.drawable.ic_clear)
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        val a = context.theme.obtainStyledAttributes(attributeSet, R.styleable.EditTextCus, 0, 0)
        try {
            regex = a.getString(R.styleable.EditTextCus_regex_pattern)
            regexx = """$regex""".toRegex()
        } finally {
            a.recycle()
        }
        setupButton(R.drawable.ic_clear)
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int) :
            super(context, attributeSet, defStyle) {
        setupButton(R.drawable.ic_clear)
    }

    fun setupButton(dra: Int) {
        deleteButtonImage = ResourcesCompat.getDrawable(resources, dra, null)!!

        setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, deleteButtonImage, null)


        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {

                    if (regexx != null && regexx!!.matches(s)) {
                        setupButton(R.drawable.ic_clear_able)
                        Log.e("TAG", "input is: $isCorrect")
                        Log.e("TAG", "Regex str is: : $regex")
                        isCorrect = true

                    } else {
                        if(isCorrect) {
                            setupButton(R.drawable.ic_clear)
                            Log.e("TAG", "input is: $isCorrect")
                            Log.e("TAG", "Regex str is: : $regex")
                            isCorrect = false
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

    }

}