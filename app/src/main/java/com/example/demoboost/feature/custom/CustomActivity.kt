package com.example.demoboost.feature.custom

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.demoboost.R

class CustomActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom)
    }
}
