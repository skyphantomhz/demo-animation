package com.example.demoboost.feature.intro

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.demoboost.R

class SlideAdapter() : PagerAdapter() {
    lateinit var layoutInflater: LayoutInflater
    val slide_image = arrayOf( R.drawable.pic1,R.drawable.pic2,R.drawable.pic3 )
    val slide_header = arrayOf( "Necessary","Pleased","Dispatched " )
    val slide_body = arrayOf(
        "Necessary ye contented newspaper zealously breakfast he prevailed. Melancholy middletons yet understood decisively boy law she",
        "Pleased him another was settled for. Moreover end horrible endeavor entrance any families. Income appear extent on of thrown in admire",
        "Dispatched entreaties boisterous say why stimulated. Certain forbade picture now prevent carried she get see sitting"
    )

    override fun getCount(): Int {
        return slide_image.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == (`object` as ConstraintLayout)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        this.layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = this.layoutInflater.inflate(R.layout.slide_layout, container, false)

        var image_view_intro = view.findViewById<ImageView>(R.id.image_view_intro)
        var text_view_title_intro = view.findViewById<TextView>(R.id.text_view_title_intro)
        var text_view_detail_intro = view.findViewById<TextView>(R.id.text_view_detail_intro)

        image_view_intro.setImageResource(slide_image[position])
        text_view_title_intro.text = slide_header[position]
        text_view_detail_intro.text = slide_body[position]

        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
//        super.destroyItem(container, position, `object`)
    }
}